// console.log("JS DOM - Manipulation");

// [SECTION] Document Object Model(DOM)
	// allows us to access or modify the properties of an HTML element in a webpage
	// it is standard on how to get, change, add or delete HTML elements
	// we will be focusing only with DOM in terms of managing forms

	// For selecting HTML elements we will be using the document.querySelector / getElementById
		// Syntax: document.querySelectorAll("html element")

		// CSS Selectors:
			// class selector (.);
			// id selector (#);
			// tag selector (html tags);
			// universal selector (*);
			// attribute selector ([attribute]);

	// querySelectorAll
	let universalSelector = document.querySelectorAll("*");
	// querySelector
	let singleUniversalSelector = document.querySelector("*");

	console.log(universalSelector);
	console.log(singleUniversalSelector);

	let classSelector = document.querySelectorAll(".full-name");
	console.log(classSelector);

	let singleClassSelector = document.querySelector(".full-name");
	console.log(singleClassSelector);

	let tagSelector = document.querySelectorAll("input");
	console.log(tagSelector);

	let spanSelector = document.querySelector("span[id]")
	console.log(spanSelector);


	// getElement
		let element = document.getElementById('fullName');
		console.log(element);

		element = document.getElementsByClassName("full-name");
		console.log(element);


// [SECTION] Event Listeners
	// whenever a user interacts with a webpage, this action is considered as an event
	// working with events is a large part of creating interactivity in a web page
	// specific function that will be triggered if the event happens

	// The function used is "addEventListener", it takes two arguments
		// first argument is a string identifying the event
		// second argument, function that the listener will trigger once the "specified event" occurs


	let txtFirstName = document.querySelector("#txt-first-name");
	// Add event listener
	txtFirstName.addEventListener("keyup", () => {
		console.log(txtFirstName.value);

		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	})

	let txtLastName = document.querySelector("#txt-last-name");

	txtLastName.addEventListener("keyup", () => {
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	})

	let fullName = document.querySelector("#fullName");
	console.log(fullName)
	let textColor = document.querySelector("#text-color");
	console.log(textColor)

	textColor.addEventListener("change", () => {
		console.log(textColor.value);
		if (textColor.value == "") {
			spanSelector.style.color = "black";
		} else {
			spanSelector.style.color = textColor.value;
		}
	})